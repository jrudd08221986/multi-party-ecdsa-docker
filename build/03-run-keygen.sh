#!/usr/bin/env bash
./bin/keygenclient --name "jack" --password "password" -u http://127.0.0.1:8332/keygen & 2>&1
echo "run second"
 sleep 1
./bin/keygenclient   --name "alice" --password "password" -u http://127.0.0.1:8333/keygen & >/dev/null 2>&1
 sleep 1
echo "run the last"
./bin/keygenclient   --name "statechain" --password "password" -u http://127.0.0.1:8334/keygen  & >/dev/null 2>&1
sleep 1
./bin/keygenclient   --name "johnny" --password "password" -u http://127.0.0.1:8335/keygen & >/dev/null 2>&1
sleep 5
echo "now kill them"
ps aux  |  grep -i keygenclient\ -k\ http  |  awk '{print $2}'  |  xargs  kill -9