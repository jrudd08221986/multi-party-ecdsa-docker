#![allow(non_snake_case)]
#![feature(proc_macro_hygiene, decl_macro)]

extern crate crypto;
extern crate curv;
extern crate multi_party_ecdsa;
extern crate paillier;
extern crate reqwest;
#[macro_use]
extern crate rocket;
extern crate base64;
extern crate rocket_contrib;

#[macro_use]
extern crate serde_derive;
extern crate hex;
extern crate rand;
extern crate secp256k1;
extern crate serde_json;

extern crate futures;
extern crate libp2p;
extern crate tokio_core;
use self::crypto::digest::Digest;
use self::crypto::sha2::Sha256;
use self::libp2p::floodsub::protocol::FloodsubMessage;
use crypto::aead::AeadDecryptor;
use crypto::aead::AeadEncryptor;
use crypto::aes::KeySize::KeySize256;
use crypto::aes_gcm::AesGcm;
use curv::arithmetic::traits::Converter;
use curv::cryptographic_primitives::proofs::sigma_dlog::DLogProof;
use curv::cryptographic_primitives::secret_sharing::feldman_vss::VerifiableSS;
use curv::elliptic::curves::traits::*;
use curv::BigInt;
use curv::{FE, GE};
use multi_party_ecdsa::net::communication::Comm;
use multi_party_ecdsa::net::communication::Error;
use multi_party_ecdsa::net::communication::SingerInfo;
use multi_party_ecdsa::net::p2pnet;
use multi_party_ecdsa::protocols::multi_party_ecdsa::gg_2018::party_i::*;
use paillier::EncryptionKey;
use rand::distributions::Alphanumeric;
use rand::{thread_rng, Rng};
use rocket::config::{Config, Environment, LoggingLevel};
use rocket::{post, State};
use rocket_contrib::json::Json;
use secp256k1::{Message, Secp256k1};
use std::collections::HashMap;
use std::env;
use std::fs;
use std::iter::repeat;
use std::process;
use std::sync::mpsc;
use std::sync::mpsc::{Receiver, Sender};
use std::sync::Mutex;

use std::thread;
use std::time::Duration;

#[derive(Clone, PartialEq, Debug, Serialize, Deserialize)]
pub struct AEAD {
    pub ciphertext: Vec<u8>,
    pub tag: Vec<u8>,
}

#[derive(Clone, PartialEq, Debug, Serialize, Deserialize)]
pub struct PartySignup {
    pub number: u32,
    pub uuid: String,
}

#[derive(Clone, PartialEq, Debug, Serialize, Deserialize)]
pub struct SigandIPMsg {
    ipaddr: String,
    signature: Vec<u8>,
}

#[derive(Clone, PartialEq, Debug, Serialize, Deserialize)]
pub struct LocalMsg {
    PubkeyArrayStr: String,
    AddressArrayStr: String,
    Privkey: String,
}

#[derive(Clone, PartialEq, Debug, Serialize, Deserialize)]
pub struct RetTssKey {
    Nodeid: String,
    Pubkeyx: String,
    Pubkeyy: String,
}

#[derive(Clone, PartialEq, Debug, Serialize, Deserialize)]
pub struct AuthenticationRet {
    signersarray: Vec<String>,
    party_num: usize,
    threshold: usize,
    mynodeidstr: String,
    //myidint is the seq of the signer on the curve.
    myidint: usize,
    peerIPmap: HashMap<String, String>,
}

pub struct LocalServerChan {
    MsgQ: Sender<LocalMsg>,
    MsgR: Receiver<RetTssKey>,
}

#[derive(Serialize, Deserialize, Clone, Default)]
pub struct Params {
    parties: String,
    threshold: String,
    bootstrapnode: String,
    signerserver: String,
    keygenserver: String,
    partynum: String,
    nodeid: String,
}
const WAITTIME: u64 = 30000;
const SECP256K1_KEYSIZE: usize = 32;
fn main() {
    if env::args().nth(4).is_some() {
        println!("too many arguments");
        return;
    }
    if env::args().nth(3).is_none() {
        println!("too few arguments");
        return;
    }

    //read parameters:
    let data = fs::read_to_string(env::args().nth(1).unwrap()).unwrap_or_default();
    if data.len() == 0 {
        errormsg("Error in get the param file".to_owned());
        process::exit(0);
    }
    let fileparams: Params = serde_json::from_str(&data).unwrap_or_default();
    if fileparams.nodeid.len() == 0 {
        errormsg("Error in parse the param file".to_owned());
        process::exit(0);
    }

    let BOOTSTRAPNODE = fileparams.bootstrapnode.clone();
    let LOCALSERVER = fileparams.keygenserver.clone();

    let localip = String::from(env::args().nth(3).unwrap());

    let addr = BOOTSTRAPNODE.to_owned();

    let (tcptx, tcprx): (mpsc::Sender<Msg>, mpsc::Receiver<Msg>) = mpsc::channel();
    thread::spawn(move || {
        p2pnet::run_tcp_server("0.0.0.0:3433".to_owned(), tcptx);
    });

    let (MsgQtx, MsgQrx): (Sender<LocalMsg>, Receiver<LocalMsg>) = mpsc::channel();
    let (MsgAtx, MsgARx): (Sender<RetTssKey>, Receiver<RetTssKey>) = mpsc::channel();

    let localserverchan = LocalServerChan {
        MsgQ: MsgQtx,
        MsgR: MsgARx,
    };

    let _ = thread::spawn(move || {
        RunLocalserver(LOCALSERVER, localserverchan);
    });
    loop {
        let party_keys = Keys::create(0 as usize);
        let sk = party_keys.u_i.get_element();
        let skptr = sk.clone().as_ptr();
        let mut p2pskarray: [u8; SECP256K1_KEYSIZE] = [0; SECP256K1_KEYSIZE];
        let slice = unsafe { std::slice::from_raw_parts(skptr, SECP256K1_KEYSIZE) };
        for i in 0..SECP256K1_KEYSIZE {
            p2pskarray[i] = slice[i];
        }
        let p2pconn = p2pnet::P2Pinstance::new(localip.as_str(), addr.as_ref(), p2pskarray);

        let (p2ptx, p2prx): (
            tokio::sync::mpsc::Sender<String>,
            tokio::sync::mpsc::Receiver<String>,
        ) = tokio::sync::mpsc::channel(1000);

        let (synctx, syncrx): (Sender<FloodsubMessage>, Receiver<FloodsubMessage>) =
            mpsc::channel();
        let (p2presptx, p2presrx): (Sender<FloodsubMessage>, Receiver<FloodsubMessage>) =
            mpsc::channel();

        thread::spawn(move || {
            p2pnet::run_p2p_server(p2pconn, p2presptx.clone(), synctx.clone(), p2prx);
        });

        KeyGen(
            &fileparams,
            localip.to_owned(),
            &party_keys,
            p2ptx.clone(),
            &MsgQrx,
            &MsgAtx,
            &p2presrx,
            &tcprx,
            &syncrx,
        );
    }
}

#[post("/keygen", format = "json", data = "<request>")]
fn ServerRecvMsg(
    request: Json<LocalMsg>,
    chan_mtx: State<Mutex<LocalServerChan>>,
) -> Json<Result<RetTssKey, ()>> {
    let msg: LocalMsg = request.0;
    let msgchan = chan_mtx.lock().unwrap();
    let ret = msgchan.MsgQ.send(msg).is_ok();
    if ret == false {
        return Json(Err(()));
    }

    match msgchan.MsgR.recv() {
        Ok(el) => return Json(Ok(el)),
        Err(_) => {
            println!("error in calculate the result");
            return Json(Err(()));
        }
    };
}

fn RunLocalserver(addr: String, MsgChannel: LocalServerChan) {
    let localserveraddr: Vec<&str> = addr.as_str().split(":").collect();

    let MsgChannel_mtx = Mutex::new(MsgChannel);

    let config = Config::build(Environment::Staging)
        .address(localserveraddr[0])
        .port(localserveraddr[1].parse::<u16>().unwrap())
        .log_level(LoggingLevel::Critical)
        .finalize()
        .unwrap();
    rocket::custom(config)
        .mount("/", routes![ServerRecvMsg])
        .manage(MsgChannel_mtx)
        .launch();
}

pub fn KeyGen(
    fileparams: &Params,
    localip: String,
    party_keys: &Keys,
    mut p2ptx: tokio::sync::mpsc::Sender<String>,
    MsgQrx: &Receiver<LocalMsg>,
    MsgAtx: &Sender<RetTssKey>,
    p2presprx: &Receiver<FloodsubMessage>,
    tcprx: &mpsc::Receiver<Msg>,
    syncrx: &Receiver<FloodsubMessage>,
) -> bool {
    let reqmsg: LocalMsg;
    match MsgQrx.recv() {
        Ok(v) => {
            reqmsg = v;
        }

        Err(_) => {
            errormsg("error in decode the message from requester".to_string());
            return false;
        }
    }

    //we need to have the greeting round to authenticate all the participants.
    let authret = match authentication(
        reqmsg,
        fileparams,
        localip.clone(),
        p2ptx.clone(),
        p2presprx,
        syncrx,
    ) {
        Ok(authret) => authret,
        Err(e) => {
            errormsg(e.to_string().to_owned());
            return false;
        }
    };
    println!("{} all signers verifitcation done!!", '\u{1F389}');

    let myidint = authret.myidint;
    let party_num = authret.party_num;
    let threshold = authret.threshold;
    let party_keys = Keys::create_from(party_keys.u_i, myidint as usize);
    let (bc_i, decom_i) = party_keys.phase1_broadcast_phase3_proof_of_correct_key();
    let wait_member_num = (party_num - 1) as usize;
    let PeerIPmap = authret.peerIPmap;
    let signersseq: Vec<usize> = (0..party_num).collect();
    let signers = authret.signersarray;
    let mynodeid = authret.mynodeidstr;
    let parames = Parameters {
        threshold: threshold as usize,
        parties_num: party_num as usize,
        bootstrapnode: fileparams.bootstrapnode.to_owned(),
        myip: localip,
        myidoncurve: myidint as u32,
        mynodeid: mynodeid.to_owned(),
    };

    //now we start the key gen process.
    let msgsend = serde_json::to_string(&bc_i).unwrap();
    //round 1 message is too large, we cannot use p2p swarm.
    for el in signersseq.iter() {
        if myidint as usize != *el {
            let msg = Comm::constructmsg(
                "round1",
                "BroadcastTss".to_owned(),
                &parames,
                msgsend.to_string(),
                parames.mynodeid.to_owned(),
                "All".to_owned(),
            );
            let value = signers[el.to_owned()].to_owned();
            let dstip = PeerIPmap.get(&value.to_owned()).unwrap();
            if *dstip == "None" {
                return false;
            }
            p2pnet::tcp_send(dstip.to_owned(), msg.to_owned());
        }
    }

    let mut round_vec = HashMap::new();
    loop {
        match tcprx.recv_timeout(Duration::from_millis(WAITTIME)) {
            Ok(resp) => {
                let body: String = resp.Body.to_string();
                let answer: Entry = serde_json::from_str(&body).unwrap();
                round_vec.insert(answer.key.senderid.to_owned(), answer);
                let length = round_vec.len();
                if length == wait_member_num as usize {
                    break;
                }
            }

            Err(e) => {
                let bombemojo = '\u{1F4A3}';
                println!("{} error in round2 p2p send {:?} ", bombemojo, e);
                break;
            }
        }
    }

    if round_vec.is_empty() {
        return false;
    }

    let mut sorted_resp_vec: Vec<String> = Vec::new();

    getsortedresult(
        msgsend.to_owned(),
        myidint as u32,
        round_vec,
        signers.to_owned(),
        signersseq.to_owned(),
        &mut sorted_resp_vec,
    );

    let mut bc1_vec: Vec<KeyGenBroadcastMessage1> = Vec::new();

    if format_vec_from_reads(&sorted_resp_vec, &mut bc1_vec).is_ok() == false {
        let skullemojio = "\u{1f480}";
        println!(
            "{} error in key generation at {}",
            skullemojio,
            "round1".to_owned()
        );
        return false;
    }

    let syncreply = waitforsync(
        "round1".to_owned(),
        wait_member_num,
        syncrx,
        p2ptx.clone(),
        "sync".to_owned(),
    );
    if syncreply == false {
        let skullemojio = "\u{1f480}";
        println!(
            "{} error in key generation at round {}",
            skullemojio,
            "round1".to_owned()
        );
        return false;
    }

    // // round 2: send ephemeral public keys and  check commitments correctness
    let msgsend = serde_json::to_string(&decom_i).unwrap();
    let msg = Comm::constructmsg(
        "round2",
        "BroadcastTss".to_owned(),
        &parames,
        msgsend.to_owned(),
        parames.mynodeid.to_owned(),
        "All".to_owned(),
    );

    if p2ptx.try_send(msg.into()).is_ok() == false {
        println!("error in send in round2");
        return false;
    }

    let round_vec = Comm::pool_msg(wait_member_num, p2presprx, "round2".to_string())
        .unwrap_or(HashMap::default());

    if round_vec.is_empty() {
        return false;
    }

    let mut sorted_resp_vec: Vec<String> = Vec::new();

    getsortedresult(
        msgsend.to_owned(),
        myidint as u32,
        round_vec,
        signers.to_owned(),
        signersseq.to_owned(),
        &mut sorted_resp_vec,
    );

    let mut decom_vec: Vec<KeyGenDecommitMessage1> = Vec::new();

    if format_vec_from_reads(&sorted_resp_vec, &mut decom_vec).is_ok() == false {
        let skullemojio = "\u{1f480}";
        println!(
            "{} error in key generation at {}",
            skullemojio,
            "round2".to_owned()
        );
        return false;
    }

    let mut y_vec: Vec<GE> = Vec::new();
    let mut enc_keys: Vec<BigInt> = Vec::new();

    for i in 0..party_num {
        let decom_j = decom_vec.get(i).unwrap();
        y_vec.push(decom_j.y_i.clone());

        enc_keys.push(
            (party_keys.y_i.clone() + decom_j.y_i.clone())
                .x_coor()
                .unwrap(),
        );
    }

    let mut y_vec_iter = y_vec.iter();
    let head = y_vec_iter.next().unwrap();
    let tail = y_vec_iter;
    let y_sum = tail.fold(head.clone(), |acc, x| acc + x);

    let (vss_scheme, secret_shares, _index) = match party_keys
        .phase1_verify_com_phase3_verify_correct_key_phase2_distribute(
            &parames, &decom_vec, &bc1_vec,
        ) {
        Ok((vss_scheme, secret_shares, _index)) => (vss_scheme, secret_shares, _index),
        Err(_) => {
            println!("invalid key {}", "\u{1f480}");
            return false;
        }
    };

    let syncreply = waitforsync(
        "round2".to_owned(),
        wait_member_num,
        syncrx,
        p2ptx.clone(),
        "sync".to_owned(),
    );
    if syncreply == false {
        let skullemojio = "\u{1f480}";
        println!(
            "{} error in key generation at round {}",
            skullemojio,
            "round2".to_owned()
        );
        return false;
    }

    //now we run the round 3
    let round = 3;
    let mut j = 0;
    for el in signersseq.iter() {
        if myidint as usize != *el {
            // prepare encrypted ss for party i:
            let key_i = BigInt::to_vec(&enc_keys[j]);
            let nonce: Vec<u8> = repeat(round).take(12).collect();
            let aad: [u8; 0] = [];
            let mut gcm = AesGcm::new(KeySize256, &key_i[..], &nonce[..], &aad);
            let plaintext = BigInt::to_vec(&secret_shares[j].to_big_int());
            let mut out: Vec<u8> = repeat(0).take(plaintext.len()).collect();
            let mut out_tag: Vec<u8> = repeat(0).take(16).collect();
            gcm.encrypt(&plaintext[..], &mut out[..], &mut out_tag[..]);
            let aead_pack_i = AEAD {
                ciphertext: out.to_vec(),
                tag: out_tag.to_vec(),
            };
            //now we send to a specific node by setting the uuid.
            let to_node = signers[el.to_owned()].to_owned();
            let msg = Comm::constructmsg(
                "round3",
                "BroadcastTss".to_owned(),
                &parames,
                serde_json::to_string(&aead_pack_i).unwrap(),
                parames.mynodeid.to_owned(),
                to_node.to_owned(),
            );
            let value = signers[el.to_owned()].to_owned();
            let dstip = PeerIPmap.get(&value.to_owned()).unwrap();
            p2pnet::tcp_send(dstip.to_owned(), msg.to_owned());
        }
        j += 1;
    }

    let mut round3_dic = HashMap::new();
    loop {
        match tcprx.recv_timeout(Duration::from_millis(WAITTIME)) {
            Ok(resp) => {
                let body: String = resp.Body.to_string();
                let answer: Entry = serde_json::from_str(&body).unwrap();

                round3_dic.insert(answer.key.senderid.to_owned(), answer);
                let length = round3_dic.len();
                if length == wait_member_num as usize {
                    break;
                }
            }

            Err(e) => {
                let bombemojo = '\u{1F4A3}';
                println!("{} error in round2 p2p send {:?} ", bombemojo, e);
                break;
            }
        }
    }

    if round3_dic.len() != wait_member_num {
        return false;
    }

    let mut party_shares: Vec<FE> = Vec::new();

    for el in signersseq.iter() {
        if el.to_owned() != myidint as usize {
            let nodeid = signers[el.to_owned()].to_owned();
            let dicdata = round3_dic.get(&nodeid).unwrap();
            let value = dicdata.value.to_owned();
            let aead_pack: AEAD = serde_json::from_str(&value).unwrap();

            let mut out: Vec<u8> = repeat(0).take(aead_pack.ciphertext.len()).collect();
            let key_i = BigInt::to_vec(&enc_keys[*el as usize]);
            let nonce: Vec<u8> = repeat(round).take(12).collect();
            let aad: [u8; 0] = [];
            let mut gcm = AesGcm::new(KeySize256, &key_i[..], &nonce[..], &aad);
            let result = gcm.decrypt(&aead_pack.ciphertext[..], &mut out, &aead_pack.tag[..]);
            assert!(result);
            let out_bn = BigInt::from(&out[..]);
            let out_fe = ECScalar::from(&out_bn);
            party_shares.push(out_fe);
        } else {
            party_shares.push(secret_shares[(*el) as usize].clone());
        }
    }
    let syncreply = waitforsync(
        "round3".to_owned(),
        wait_member_num,
        syncrx,
        p2ptx.clone(),
        "sync".to_owned(),
    );
    if syncreply == false {
        let skullemojio = "\u{1f480}";
        println!(
            "{} error in signature generation at round {}",
            skullemojio,
            "round3".to_owned()
        );
        return false;
    }

    // round 4: send vss commitments
    let msgsend = serde_json::to_string(&vss_scheme).unwrap();

    let msg = Comm::constructmsg(
        "round4",
        "BroadcastTss".to_owned(),
        &parames,
        msgsend.to_owned(),
        parames.mynodeid.to_owned(),
        "All".to_owned(),
    );

    if p2ptx.try_send(msg.into()).is_ok() == false {
        println!("error in send in round4");
        return false;
    }

    let round_vec = Comm::pool_msg(wait_member_num, p2presprx, "round4".to_string())
        .unwrap_or(HashMap::default());

    if round_vec.is_empty() {
        return false;
    }
    let mut sorted_resp_vec: Vec<String> = Vec::new();

    getsortedresult(
        msgsend.to_owned(),
        myidint as u32,
        round_vec,
        signers.to_owned(),
        signersseq.to_owned(),
        &mut sorted_resp_vec,
    );

    let mut vss_scheme_vec: Vec<VerifiableSS> = Vec::new();

    if format_vec_from_reads(&sorted_resp_vec, &mut vss_scheme_vec).is_ok() == false {
        let skullemojio = "\u{1f480}";
        println!(
            "{} error in signature generation at {}",
            skullemojio,
            "round2".to_owned()
        );
        return false;
    }

    let (shared_keys, dlog_proof) = match party_keys
        .phase2_verify_vss_construct_keypair_phase3_pok_dlog(
            &parames,
            &y_vec,
            &party_shares,
            &vss_scheme_vec,
            &((myidint + 1) as usize),
        ) {
        Ok((shared_keys, dlog_proof)) => (shared_keys, dlog_proof),
        Err(_) => {
            println!("invalid vss {}", "\u{1f480}");
            return false;
        }
    };

    let syncreply = waitforsync(
        "round4".to_owned(),
        wait_member_num,
        syncrx,
        p2ptx.clone(),
        "sync".to_owned(),
    );
    if syncreply == false {
        let skullemojio = "\u{1f480}";
        println!(
            "{} error in signature generation at round {}",
            skullemojio,
            "round4".to_owned()
        );
        return false;
    }

    // round 5: send vss commitments

    let msgsend = serde_json::to_string(&dlog_proof).unwrap();
    let msg = Comm::constructmsg(
        "round5",
        "BroadcastTss".to_owned(),
        &parames,
        msgsend.to_owned(),
        parames.mynodeid.to_owned(),
        "All".to_owned(),
    );

    if p2ptx.try_send(msg.into()).is_ok() == false {
        println!("error in send in round5");
        return false;
    }

    let round_vec = Comm::pool_msg(wait_member_num, p2presprx, "round5".to_string())
        .unwrap_or(HashMap::default());

    if round_vec.is_empty() {
        return false;
    }

    let mut sorted_resp_vec: Vec<String> = Vec::new();

    getsortedresult(
        msgsend.to_owned(),
        myidint as u32,
        round_vec,
        signers.to_owned(),
        signersseq.to_owned(),
        &mut sorted_resp_vec,
    );

    let mut dlog_proof_vec: Vec<DLogProof> = Vec::new();

    if format_vec_from_reads(&sorted_resp_vec, &mut dlog_proof_vec).is_ok() == false {
        let skullemojio = "\u{1f480}";
        println!(
            "{} error in signature generation at {}",
            skullemojio,
            "round5".to_owned()
        );
        return false;
    }

    let flag = Keys::verify_dlog_proofs(&parames, &dlog_proof_vec, &y_vec).is_ok();
    if flag == false {
        println!("bad dlog proof");
        return false;
    }
    // //////////////////////////////////////////////////////////////////////////////
    // //save key to file:
    //
    let paillier_key_vec = (0..party_num)
        .map(|i| bc1_vec[i as usize].e.clone())
        .collect::<Vec<EncryptionKey>>();

    let keygen_json = serde_json::to_string(&(
        party_keys,
        shared_keys,
        myidint as u32,
        vss_scheme_vec,
        paillier_key_vec,
        y_sum,
    ))
    .unwrap();

    let rettsskey = RetTssKey {
        Nodeid: (myidint + 1).to_string().to_owned(),
        Pubkeyx: y_sum.x_coor().unwrap().to_str_radix(10),
        Pubkeyy: y_sum.y_coor().unwrap().to_str_radix(10),
    };
    if MsgAtx.send(rettsskey).is_ok() {
        let erroremojio = "\u{1f432}";
        let martini_emoji = "'\u{1F378}";
        println!("{} Tss keygen done {}", erroremojio, martini_emoji);
    } else {
        return false;
    }

    fs::write(env::args().nth(2).unwrap(), keygen_json).expect("Unable to save  keyfile!");

    let storeparams = Params {
        partynum: (parames.myidoncurve + 1).to_string(),
        parties: party_num.to_string(),
        threshold: parames.threshold.to_string(),
        bootstrapnode: fileparams.bootstrapnode.to_owned(),
        signerserver: fileparams.signerserver.to_owned(),
        keygenserver: fileparams.keygenserver.to_owned(),
        nodeid: mynodeid,
    };
    let parameterjson = serde_json::to_string(&storeparams).unwrap();
    fs::write(env::args().nth(1).unwrap(), parameterjson).expect("Unable to save parameter file!");

    println!("key generation is done!");

    let msg = Comm::constructmsg(
        "CLOSEP2P",
        "BroadcastTss".to_owned(),
        &parames,
        "RESERVED".to_owned(),
        parames.mynodeid.to_owned(),
        "All".to_owned(),
    );
    if p2ptx.try_send(msg.into()).is_ok() == false {
        println!("error in close p2p connection");
        return false;
    }
    return true;
}

fn getsortedresult(
    myvalue: String,
    party_num_int: u32,
    inputdic: HashMap<String, Entry>,
    signers: Vec<String>,
    signersseq: Vec<usize>,
    ans_vec: &mut Vec<String>,
) {
    for el in signersseq.iter() {
        if el.to_owned() == party_num_int as usize {
            ans_vec.push(myvalue.to_owned());
        } else {
            let nodeid = signers[el.to_owned()].to_owned();
            let dicdata = inputdic.get(&nodeid).unwrap();
            let value = dicdata.value.to_owned();
            ans_vec.push(value);
        }
    }
}

fn format_vec_from_reads<'a, T: serde::Deserialize<'a> + Clone>(
    ans_vec: &'a Vec<String>,
    new_vec: &'a mut Vec<T>,
) -> Result<bool, serde_json::Error> {
    let mut previous: String = "aa".to_owned();
    for elem in ans_vec.iter() {
        match serde_json::from_str(&elem) {
            Ok(value) => {
                new_vec.push(value);
                previous = elem.to_owned();
            }
            Err(e) => {
                let erroremojio = "\u{1f957}";
                println!(
                    "{} failed to covert string {:?} with error {:?}",
                    erroremojio, elem, e
                );
                println!("correct---->{:?}", previous.to_owned());
                return Err(e);
            }
        }
    }
    return Ok(true);
}

fn waitforsync(
    round: String,
    parties: usize,
    rx: &mpsc::Receiver<FloodsubMessage>,
    mut p2ptx: tokio::sync::mpsc::Sender<String>,
    topic: String,
) -> bool {
    let syncmsg = Comm::syncmsg(round.to_owned(), topic, "ok".to_owned());

    if p2ptx.try_send(syncmsg.into()).is_ok() == false {
        println!("error in send in round1");
        return false;
    }

    return Comm::pool_sync_reply(parties, rx, round.to_owned()).is_ok();
}

fn authentication(
    reqmsg: LocalMsg,
    fileparams: &Params,
    localip: String,
    mut p2ptx: tokio::sync::mpsc::Sender<String>,
    p2presprx: &Receiver<FloodsubMessage>,
    syncrx: &Receiver<FloodsubMessage>,
) -> Result<AuthenticationRet, Error> {
    let mut signers: Vec<SingerInfo> = Vec::new();
    let pubkeys: Vec<Vec<u8>> = reqmsg
        .PubkeyArrayStr
        .split(",")
        .map(|x| base64::decode(x).unwrap_or_default())
        .collect();

    for value in pubkeys {
        let end = value.len() - 1;
        let start = end - secp256k1::constants::PUBLIC_KEY_SIZE + 1;
        let pubkey: Vec<u8> = value[start..].iter().cloned().collect();
        let mut hasher = Sha256::new();
        // write input message
        hasher.input(pubkey.as_slice());
        let mut publickeyhashsvec = vec![0; hasher.output_bytes()];
        // // read hash digest
        hasher.result(&mut publickeyhashsvec);
        let thispeer = SingerInfo {
            publickey: pubkey,
            keyhash: hex::encode(publickeyhashsvec),
            myidint: 0,
            sigverfy: false,
        };
        signers.push(thispeer);
    }

    signers.sort_by(|a, b| a.keyhash.to_lowercase().cmp(&b.keyhash.to_lowercase()));

    for (i, item) in signers.iter_mut().enumerate() {
        item.myidint = i;
    }

    let importskhex = base64::decode(&reqmsg.Privkey).unwrap_or_default();
    if importskhex.len() == 0 {
        return Err(Error::InvalidSecretKey);
    }

    let imprtkey = hex::decode(importskhex).unwrap();

    let mysk = match secp256k1::SecretKey::from_slice(imprtkey.as_slice()) {
        Ok(mysk) => mysk,
        Err(_) => return Err(Error::InvalidSecretKey),
    };

    let secp = Secp256k1::new();
    let mypubkey = secp256k1::PublicKey::from_secret_key(&secp, &mysk);
    let pubarr = mypubkey.serialize();

    let myself = match signers
        .iter()
        .find(|x| x.publickey.iter().zip(pubarr.iter()).all(|(a, b)| a == b))
    {
        None => return Err(Error::InvalidSigner),
        Some(myself) => myself.clone(),
    };

    let myidint = myself.myidint as usize;
    let party_num = signers.len() as usize;
    let threshold_temp = (party_num as f64 * 2.0) / 3.0;
    let threshold = threshold_temp.ceil() as usize - 1;
    println!(
        "we have {} parties, my seq is {}, threshold is {}",
        party_num, myidint, threshold
    );

    let wait_member_num = (party_num - 1) as usize;

    let parames = Parameters {
        threshold: threshold as usize,
        parties_num: party_num as usize,
        bootstrapnode: fileparams.bootstrapnode.to_owned(),
        myip: localip,
        myidoncurve: myidint as u32,
        mynodeid: myself.keyhash.to_owned(),
    };

    let challengestring: String = thread_rng().sample_iter(&Alphanumeric).take(30).collect();
    let msg = Comm::constructmsg(
        "roundgreeting",
        "BroadcastTss".to_owned(),
        &parames,
        challengestring.to_owned(),
        myself.keyhash.to_owned(),
        "All".to_owned(),
    );

    if p2ptx.try_send(msg.into()).is_ok() == false {
        println!("error in send in roundgreeting");
        return Err(Error::ConnectionTimeout);
    }
    let resp = Comm::pool_membermsg(signers.clone(), p2presprx, "roundgreeting".to_string())
        .unwrap_or(HashMap::default());
    if resp.is_empty() {
        return Err(Error::ConnectionTimeout);
    }

    let mut challenges: Vec<String> = Vec::new();
    challenges.push(challengestring);
    for (_, value) in resp {
        challenges.push(value.value.to_owned());
    }

    let syncreply = waitforsync(
        "roundgreeting".to_owned(),
        wait_member_num,
        syncrx,
        p2ptx.clone(),
        "sync".to_owned(),
    );
    if syncreply == false {
        let skullemojio = "\u{1f480}";
        println!(
            "{} error in key generation at round {}",
            skullemojio,
            "roundgreeting1".to_owned()
        );
        return Err(Error::SyncTimeout);
    }
    challenges.sort_by(|a, b| a.to_lowercase().cmp(&b.to_lowercase()));

    let mut msg: String = "".to_string();
    for val in challenges.iter() {
        msg += val;
    }
    let mut hashermsg = Sha256::new();

    hashermsg.input(msg.as_bytes());
    let mut msghashsvec = vec![0; hashermsg.output_bytes()];
    // // read hash digest
    hashermsg.result(&mut msghashsvec);

    let signmessage = Message::from_slice(msghashsvec.as_slice()).expect("32 bytes");
    let sig = secp.sign(&signmessage, &mysk);
    let sigcompact = sig.serialize_compact();
    let sendmsg: SigandIPMsg = SigandIPMsg {
        ipaddr: parames.myip.to_owned(),
        signature: sigcompact.to_vec(),
    };

    let msg = Comm::constructmsg(
        "roundgreeting2",
        "BroadcastTss".to_owned(),
        &parames,
        serde_json::to_string(&sendmsg).unwrap().to_owned(),
        myself.keyhash.to_owned(),
        "All".to_owned(),
    );

    if p2ptx.try_send(msg.into()).is_ok() == false {
        println!("error in send in roundgreeting2");
        return Err(Error::ConnectionTimeout);
    }

    let resp = Comm::pool_msg(wait_member_num, p2presprx, "roundgreeting2".to_string())
        .unwrap_or(HashMap::default());
    if resp.is_empty() {
        return Err(Error::ConnectionTimeout);
    }

    let mut peerIPmap = HashMap::new();
    for (senderid, value) in resp {
        let targetsigner = signers.iter().find(|x| x.keyhash == senderid).unwrap();
        let pubkey = targetsigner.publickey.as_slice();
        let secppubkey = match secp256k1::PublicKey::from_slice(pubkey) {
            Ok(secppubkey) => secppubkey,
            Err(_) => {
                return Err(Error::InvalidPublicKey);
            }
        };
        let msgpackstr = value.value.to_owned();
        let msgpack: SigandIPMsg = match serde_json::from_str(msgpackstr.as_str()) {
            Ok(msgpack) => msgpack,
            Err(_) => {
                errormsg("error in decode the received signature".to_owned());
                return Err(Error::IncorrectSignature);
            }
        };
        if VerifySignature(
            msgpack.signature.clone(),
            signmessage.to_owned(),
            secppubkey,
        ) {
            match signers.iter_mut().find(|x| x.keyhash == senderid) {
                None => continue,
                Some(x) => {
                    x.sigverfy = true;
                }
            };

            let peerip: String = value.value.to_owned();
            let v: Vec<&str> = peerip.split('/').collect();
            let mut addr = v[2].to_owned();
            addr.push(':');
            // fixme default port for TCP
            addr.push_str("3433");
            peerIPmap.insert(senderid.to_string(), addr);
        } else {
            return Err(Error::InvalidSigner);
        }
    }

    let syncreply = waitforsync(
        "roundgreeting2".to_owned(),
        wait_member_num,
        syncrx,
        p2ptx.clone(),
        "sync".to_owned(),
    );
    if syncreply == false {
        let skullemojio = "\u{1f480}";
        println!(
            "{} error in key generation at round {}",
            skullemojio,
            "roundgreeting2".to_owned()
        );
        return Err(Error::SyncTimeout);
    }
    let mut signersarray: Vec<String> = Vec::new();
    for el in signers.iter() {
        if el.sigverfy || el.keyhash.clone() == myself.keyhash.clone() {
            signersarray.push(el.keyhash.clone());
        }
    }
    if signersarray.len() != party_num {
        return Err(Error::NotEnoughSigner);
    }
    let authret = AuthenticationRet {
        signersarray: signersarray,
        party_num: party_num,
        threshold: threshold,
        myidint: myidint,
        peerIPmap: peerIPmap,
        mynodeidstr: myself.keyhash,
    };

    return Ok(authret);
}

fn VerifySignature(sigvec: Vec<u8>, msg: Message, pubkey: secp256k1::PublicKey) -> bool {
    let remotesignatures = match secp256k1::Signature::from_compact(&sigvec) {
        Ok(remotesignatures) => remotesignatures,
        Err(_) => {
            errormsg("Error in check the message".to_owned());
            return false;
        }
    };

    let secp = Secp256k1::new();

    return secp.verify(&msg, &remotesignatures, &pubkey).is_ok();
}

fn errormsg(msg: String) {
    let bombemojo = '\u{1F4A3}';
    println!("{} error in waiting for message {:?} ", bombemojo, msg);
}
