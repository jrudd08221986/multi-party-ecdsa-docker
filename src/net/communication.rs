extern crate futures;
extern crate libp2p;
extern crate tokio_core;
use self::libp2p::floodsub::protocol::FloodsubMessage;
use protocols::multi_party_ecdsa::gg_2018::party_i::*;
use std::collections::HashMap;
use std::fmt;
use std::sync::mpsc;
use std::sync::mpsc::RecvTimeoutError;
use std::time::Duration;
const WAITTIME: u64 = 10000;
pub struct Comm {}
#[derive(Clone, PartialEq, Debug, Serialize, Deserialize)]
pub struct SingerInfo {
    pub publickey: Vec<u8>,
    pub keyhash: String,
    pub myidint: usize,
    pub sigverfy: bool,
}

/// An ECDSA error
#[derive(Copy, PartialEq, Eq, Clone, Debug)]
pub enum Error {
    /// Signature failed verification
    IncorrectSignature,

    /// Bad public key
    AuthenticationError,
    /// Bad signature
    InvalidSignature,
    InvalidSigner,
    /// P2P or Tcp communication error
    ConnectionTimeout,
    InvalidPublicKey,
    InvalidSecretKey,
    NotEnoughSigner,
    /// sync error
    SyncTimeout,
}

impl Error {
    fn as_str(&self) -> &str {
        match *self {
            Error::IncorrectSignature => "secp: signature failed verification",
            Error::InvalidPublicKey => "secp: malformed public key",
            Error::InvalidSignature => "secp: malformed signature",
            Error::InvalidSecretKey => "secp: malformed or out-of-range secret key",
            Error::ConnectionTimeout => "pull message timeout",
            Error::SyncTimeout => "signer sync error",
            Error::NotEnoughSigner => "not enough signer",
            Error::AuthenticationError => "authentication failed",
            Error::InvalidSigner => "invalid signer",
        }
    }
}

// Passthrough Debug to Display, since errors should be user-visible
impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        f.write_str(self.as_str())
    }
}

#[cfg(feature = "std")]
impl std::error::Error for Error {
    fn description(&self) -> &str {
        self.as_str()
    }
}

impl Comm {
    pub fn syncmsg(round: String, topic: String, status: String) -> String {
        let msg = Msg {
            Head: round.to_owned(),
            Body: status.to_owned(),
            Topic: topic.to_owned(),
        };

        let msgstr: String = serde_json::to_string(&msg).unwrap();

        return msgstr;
    }

    pub fn constructmsg(
        round: &str,
        topic: String,
        parames: &Parameters,
        data: String,
        senderid: String,
        receiverid: String,
    ) -> String {
        let key = TupleKey {
            first: parames.myidoncurve.to_string(),
            second: round.to_string(),
            third: "RESERVED".to_owned(),
            senderid: senderid.to_owned(),
            recvid: receiverid.to_owned(),
        };
        let entry = Entry {
            key: key.clone(),
            value: data,
        };
        let body: String = serde_json::to_string(&entry).unwrap();

        let msg = Msg {
            Head: round.to_string(),
            Body: body,
            Topic: topic,
        };

        let msgstr: String = serde_json::to_string(&msg).unwrap();

        return msgstr;
    }

    pub fn pool_sync_reply(
        parties: usize,
        rx: &mpsc::Receiver<FloodsubMessage>,
        roundwanted: String,
    ) -> Result<bool, RecvTimeoutError> {
        let mut length = 0;
        loop {
            match rx.recv_timeout(Duration::from_millis(WAITTIME)) {
                Ok(result) => {
                    let messagebody = result.data;
                    let answer: Msg = serde_json::from_slice(&messagebody).unwrap();
                    let body: String = answer.Body.to_string();
                    let head: String = answer.Head;
                    if head == roundwanted && body == "ok".to_owned() {
                        length += 1;
                    } else {
                        return Ok(false);
                    }
                    if length == parties {
                        break;
                    }
                }

                Err(e) => {
                    let bombemojo = '\u{1F4A3}';
                    println!("{} error in create the pool sync {:?} ", bombemojo, e);
                    return Err(e);
                }
            }
        }
        return Ok(true);
    }

    pub fn pool_membermsg(
        signers: Vec<SingerInfo>,
        rx: &mpsc::Receiver<FloodsubMessage>,
        roundwanted: String,
    ) -> Result<HashMap<String, Entry>, RecvTimeoutError> {
        let mut peer_reply = HashMap::new();
        //we have n-1
        let numparties = signers.len() - 1;
        loop {
            match rx.recv_timeout(Duration::from_millis(WAITTIME)) {
                Ok(result) => {
                    let messagebody = result.data;
                    let answer: Msg = serde_json::from_slice(&messagebody).unwrap();
                    let body: String = answer.Body.to_string();
                    let answer: Entry = serde_json::from_str(&body).unwrap();
                    if answer.key.second != roundwanted {
                        println!(
                            "{} is different from wanted {}",
                            answer.key.second, roundwanted
                        );
                        return Err(RecvTimeoutError::Timeout);
                    }
                    let senderidstr = answer.key.senderid.to_string();
                    match signers.iter().find(|x| x.keyhash == senderidstr) {
                        None => continue,
                        Some(_) => {
                            peer_reply.insert(answer.key.senderid.to_string(), answer);
                            let length = peer_reply.len();
                            if length == numparties {
                                break;
                            }
                        }
                    };
                }

                Err(e) => {
                    let bombemojo = '\u{1F4A3}';
                    println!("{} error in create the signature {:?} ", bombemojo, e);
                    return Err(e);
                }
            }
        }

        return Ok(peer_reply);
    }

    pub fn pool_msg(
        parties: usize,
        rx: &mpsc::Receiver<FloodsubMessage>,
        roundwanted: String,
    ) -> Result<HashMap<String, Entry>, RecvTimeoutError> {
        let mut peer_reply = HashMap::new();
        loop {
            match rx.recv_timeout(Duration::from_millis(WAITTIME)) {
                Ok(result) => {
                    let messagebody = result.data;
                    let answer: Msg = serde_json::from_slice(&messagebody).unwrap();
                    let body: String = answer.Body.to_string();
                    let answer: Entry = serde_json::from_str(&body).unwrap();
                    if answer.key.second != roundwanted {
                        println!(
                            "{} is different from wanted {}",
                            answer.key.second, roundwanted
                        );
                        return Err(RecvTimeoutError::Timeout);
                    }
                    peer_reply.insert(answer.key.senderid.to_string(), answer);
                    let length = peer_reply.len();
                    if length == parties {
                        break;
                    }
                }

                Err(e) => {
                    let bombemojo = '\u{1F4A3}';
                    println!("{} error in create the signature {:?} ", bombemojo, e);
                    return Err(e);
                }
            }
        }

        return Ok(peer_reply);
    }
}
